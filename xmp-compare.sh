#!/bin/bash

directory_path="./"

# Function to calculate display length of a string (considering multibyte characters)
display_length() {
    echo "$1" | awk '{print length}'
}

# Check if xmllint is installed
if ! command -v xmllint &> /dev/null
then
    echo "xmllint could not be found. Please install libxml2-utils."
    exit 1
fi

declare -A modules
declare -A file_module_map
declare -A filenames
declare -A module_counts
declare -A file_module_count # New array to count modules for each file

# Variables for formatting
header_title="Módulo"
max_module_length=$(display_length "$header_title")
max_filename_length=0
max_count_length=2 # Length of "#" column

# Process each XMP file in the directory
for file_path in "$directory_path"/*.xmp; do
    filename=$(basename "$file_path" .xmp)
    filenames[$filename]=1
    max_filename_length=$(($(display_length "$filename") > max_filename_length ? $(display_length "$filename") : max_filename_length))

    # Extract the history entries
    while IFS= read -r line; do
        operation=$(echo "$line" | grep -oP 'operation="\K[^"]+')
        if [ ! -z "$operation" ]; then
            if [[ ! ${file_module_map["$filename,$operation"]} ]]; then
                modules[$operation]=1
                file_module_map["$filename,$operation"]=1
                ((module_counts[$operation]++))
                ((file_module_count[$filename]++)) # Increment module count for the file
                max_module_length=$(($(display_length "$operation") > max_module_length ? $(display_length "$operation") : max_module_length))
            fi
        fi
    done < <(xmllint --xpath "//*[local-name()='li'][@*[local-name()='enabled']='1']/@*[local-name()='operation']" "$file_path" 2>/dev/null)
done

# Sort modules based on frequency
sorted_modules=($(for module in "${!module_counts[@]}"; do echo "$module ${module_counts[$module]}"; done | sort -k2 -nr | cut -d' ' -f1))

# Function to print padded text for markdown
print_padded() {
    printf "| %-${1}s " "$2"
}

# Function to print separator for markdown
print_separator() {
    local length=$(( $1 + 2 ))
    printf "|"
    for i in $(seq 1 $length); do printf "-"; done
}

# Print table header
print_padded $max_module_length "$header_title"
print_padded $max_count_length "#"
for filename in "${!filenames[@]}"; do
    print_padded $max_filename_length "$filename"
done
echo "|"

# Print separator
print_separator $max_module_length
print_separator $max_count_length
for filename in "${!filenames[@]}"; do
    print_separator $max_filename_length
done
echo "|"

# Print table rows
for module in "${sorted_modules[@]}"; do
    print_padded $max_module_length "$module"
    print_padded $max_count_length "${module_counts[$module]}"
    for filename in "${!filenames[@]}"; do
        if [[ ${file_module_map["$filename,$module"]} ]]; then
            print_padded $max_filename_length " x "
        else
            print_padded $max_filename_length " - "
        fi
    done
    echo "|"
done

# Print the last row for total module count per file
print_padded $max_module_length "# módulos usados"
print_padded $max_count_length "-"
for filename in "${!filenames[@]}"; do
    print_padded $max_filename_length "${file_module_count[$filename]}"
done
echo "|"

