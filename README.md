# xmp-compare

Pequeño script diseñado para comparar los ficheros xmp de procesado de [Darktable](https://www.darktable.org) de varios usuarios.

Muestra que módulos han sido utilizado por que usuario, ordenándolo poe el mayor número de usuarios que lo tengan activo.

Lee todos los ficheros `xmp` de una carpeta y considera el nombre del fichero como el usuario.

Para usarlo simplemente ejecutalo en una carpeta que contenga varios ficheros `xmp`. 

## Instrucciones

Solo ejecuta el fichero `xmp-compare.sh` en una carpeta que contenga ficheros `xmp` de darktable.

Este pensando para funcionar en Linux.


```bash
./xmp-compare.sh
```

## Ejemplo

Muestra los resultados en una tabla comparativa

Por ejemplo, si tenemos 3 ficheros xmp, `usuario1.xmp`, `usuario2.xmp`y `usuario3.xmp` genera la siguiente tabla:

| Módulo           | #  | usuario1    | usuario2   | usuario3     |
|------------------|----|-------------|-------------|-------------|
| temperature      | 3  |  x          |  x          |  x          |
| rawprepare       | 3  |  x          |  x          |  x          |
| highlights       | 3  |  x          |  x          |  x          |
| gamma            | 3  |  x          |  x          |  x          |
| flip             | 3  |  x          |  x          |  x          |
| filmicrgb        | 3  |  x          |  x          |  x          |
| exposure         | 3  |  x          |  x          |  x          |
| demosaic         | 3  |  x          |  x          |  x          |
| colorin          | 3  |  x          |  x          |  x          |
| colorbalancergb  | 3  |  x          |  x          |  x          |
| channelmixerrgb  | 3  |  x          |  x          |  x          |
| toneequal        | 2  |  x          |  x          |  -          |
| lens             | 2  |  x          |  x          |  -          |
| diffuse          | 2  |  x          |  x          |  -          |
| colorout         | 2  |  x          |  -          |  x          |
| retouch          | 1  |  -          |  -          |  x          |
| dither           | 1  |  -          |  x          |  -          |
| denoiseprofile   | 1  |  x          |  -          |  -          |
| bilat            | 1  |  -          |  -          |  x          |
| atrous           | 1  |  x          |  -          |  -          |
| ashift           | 1  |  -          |  -          |  x          |
| # módulos usados | -  | 17          | 15          | 15          |

